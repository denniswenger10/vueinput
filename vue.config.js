const SassGenerator = require('@wentools/sass-generator')
sassGenerator = new SassGenerator

sassGenerator.variables = {
  // Everything you create here will become a Sass-variable.
  // Must be object-properties. Not arrays.
  fontsize: {
    small: '15px',
    big: '25px'
  },
  colour: {
    black: "rgb(25,25,25)",
    gray: {
      light: "rgb(190,190,190)",
      mid: "rgb(130,130,130)",
      dark: "rgb(65,65,65)"
    },
    blue: { 
      light: "rgb(60,150,230)", // Sass variable will be: $colour_blue_light: rgb(60,150,230);
      mid: "rgb(20,90,200)"
    }
  }
}

sassGenerator.defaultFont = 'Quicksand'
sassGenerator.path = './src/assets/styles'
sassGenerator.fileName = 'globalStyles'

sassGenerator.updateFiles()
// Mitt egna byggverktyg körs här ovan. Eftersom att det körs här och
// inte i webpack så uppdateras inte byggningen vid sparning.
// Men gjorde så just här. Kommer bestämma hur jag vill baka in det i byggningen senare.

module.exports = {
  css: {
    loaderOptions: {
      sass: {
        // Här under importes den automatiskt byggda sass-filen
        data: `
          @import "@/assets/styles/globalStyles.scss";
        `
      }
    }
  }
}