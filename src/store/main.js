import Vue from 'vue'
import Vuex from 'vuex'
import router from '@/router'

Vue.use(Vuex)

// Detta är Vuex. Det fyller samma funktion som Redux gör i React.
// Poängen är att ha states på ett ställe, single source of truth. 
// Hade det varit en större applikation med t.ex. användarinformation eller så
// så hade jag delat upp denna fil. Till exempel ha en userStore. 

export default new Vuex.Store({
  state: {
    applicationName: 'ComponentLibrary',
    links: [
      { title: 'Home', link: '/', active: true },
      { title: 'About', link: '/about', active: false },
    ]
  },
  mutations: {
    setActiveLink(state, link) {
      
      router.push(link)
      
      state.links.map(item => {
        item.link == link ? item.active = true : item.active = false
        return item
      })

    }
  },
  actions: {
    // Om jag skulle hämta data någonstans så skulle jag kanske göra det här. Om det var en enkel app.
    // Om appen var större och mer komplex så skulle jag troligen hämta i webworkers och abstrahera bort det mer.
  }
})
