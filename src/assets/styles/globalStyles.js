export default { fontsize: { small: '15px', big: '25px' },
  colour:
   { black: 'rgb(25,25,25)',
     gray:
      { light: 'rgb(190,190,190)',
        mid: 'rgb(130,130,130)',
        dark: 'rgb(65,65,65)' },
     blue: { light: 'rgb(60,150,230)', mid: 'rgb(20,90,200)' } } }